package com.ufro.Dees.models;

import lombok.Data;

import javax.persistence.*;
import java.util.List;

@Entity
@Data

public class Usuario {
    @Id
    @GeneratedValue
    private long id;
    private String nombre;
    private String email;
    private String contraseña;
    @OneToMany(mappedBy = "usuario")
    private List<Imagen> imagenes;

}
