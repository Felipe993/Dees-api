package com.ufro.Dees.models;
import lombok.Data;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
public class Imagen {
    @Id
    @GeneratedValue
    private long id;
    private int vistas;
    private String nombre;
    @ManyToOne
    @JoinColumn(name="usuario_id")
    private Usuario usuario;


}
