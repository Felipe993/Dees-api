package com.ufro.Dees.controllers;

import com.ufro.Dees.models.Imagen;
import com.ufro.Dees.repositories.ImagenRepository;
import com.ufro.Dees.services.ImagenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ImagenController {
    @Autowired
    private ImagenService imagenService;
    @Autowired
    private ImagenRepository imagenRepository;

    @GetMapping("/listarImagen")
    public List<Imagen> listarImagen(){
        return imagenService.mostrarImagenes();
    }
}
