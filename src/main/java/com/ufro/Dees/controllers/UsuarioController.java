package com.ufro.Dees.controllers;

import com.ufro.Dees.models.Usuario;
import com.ufro.Dees.repositories.ImagenRepository;
import com.ufro.Dees.repositories.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UsuarioController {
    @Autowired
    private UsuarioRepository usuarioRepository;
    @Autowired
    private ImagenRepository imagenRepository;

    @PostMapping("/crearUsuario")
    public Usuario crearUsuario(Usuario usuario){
        return usuarioRepository.save(usuario);
    }
}
