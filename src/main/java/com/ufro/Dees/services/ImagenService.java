package com.ufro.Dees.services;

import com.ufro.Dees.models.Imagen;
import com.ufro.Dees.repositories.ImagenRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ImagenService {
    @Autowired
    ImagenRepository imagenRepository;

    public List<Imagen> mostrarImagenes(){
        List<Imagen>imagenes =new ArrayList<>();
        imagenes =imagenRepository.findAll();
        return imagenes;
    }
}
