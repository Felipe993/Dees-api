package com.ufro.Dees.repositories;
import com.ufro.Dees.models.Imagen;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

public interface ImagenRepository extends JpaRepository<Imagen, Long> {

}
