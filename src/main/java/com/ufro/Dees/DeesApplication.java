package com.ufro.Dees;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DeesApplication {

	public static void main(String[] args) {
		SpringApplication.run(DeesApplication.class, args);
	}

}
